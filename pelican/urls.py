from django.conf.urls import url
from . import views
from django.urls import include, re_path, path

# work with djando version < 2.0.2
# urlpatterns = [
#     url(r'^$', views.get_name, name='pelican'),
#     url(r'^thanks/$', views.thanks, name='thank'),
#     url(r'^comment/(?P<pk>\d+)/$', views.datail_comment, name='datail_comment'),
#     url(r'^extend/$', views.extend, name='extend'),
#     url(r'^delete/(?P<pk>\d+)/$', views.delete_comment, name='delete_comment'),
# ]


urlpatterns = [
    path('', views.get_name, name='pelican'),
    path('thanks/', views.thanks, name='thank'),
    # for text use <slug:title> ex: 'articles/<slug:title>/<int:section>/ 
    path('comment/<int:pk>/<slug:name>/', views.datail_comment, name='datail_comment'),
    path('extend/', views.extend, name='extend'),
    path('delete/<int:pk>/', views.delete_comment, name='delete_comment'),

]