from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect


from .forms import NameForm
from .upload_file import handle_uploaded_file
from .icomment import IComment


def get_name(request):
    """
    control address 
    processing GET and POST methods
    Args:
        request: str, request from the user
    Raises:
    Returns:
        render page: if user send GET method with form
        redirect to new page: if user send POST method
    """
    # if use method POST
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        
        c = IComment()
        form = NameForm(request.POST, request.FILES)

        # check whether it's valid:
        if form.is_valid():
            # get data from form with method "cleaned_data"
            name = form.cleaned_data['your_name']
            comment = form.cleaned_data['comment']
            email = form.cleaned_data['email_address']
            like = form.cleaned_data['like']
            # handle_uploaded_file(request.FILES['file'])

            c.create_comment(
                name=name,
                comment=comment,
                email=email,
                like=like
            )

            # redirect to a new URL:
            # return HttpResponseRedirect('/thanks/')
            # return redirect('/thanks/')

            # redirect to a new view
            # NOTE! the name of the view is contained in the file "urls.py"
            return redirect('thank')

    # if use method GET
    else:
        form = NameForm()

    return render(request, 'pelican/pelican_template.html', {'form': form})

def thanks(request):
    """
    thank view 
    Args:
        request: str, request from user
    Return:
        render page: if user send GET method
    """
    c = IComment()
    comments = c.get_all_comments()
    return render(request, 'pelican/thanks_template.html', {'comments': comments})


#
# work with extend base and  html
#
from .models import Comment
def extend(request):
    comment = get_object_or_404(Comment, pk=1)
    return render(request, 'pelican/extend.html', {'comment': comment})

#
# work with dynamic URL 
#
from django.shortcuts import get_object_or_404
from .models import Comment
def datail_comment(request, pk, name):
    """
    dynamic URL address is formed as "/comment/1/"
    Args:
        pk: str, primary key for storage
        name: str, username
    Return:
        render page: if user send GET method
    """
    # if not found MongoDB jbject with pk return 404
    comment = get_object_or_404(Comment, pk=pk)
    return render(request, 'pelican/datail_comment.html', {'comment': comment})

def delete_comment(request, pk):
    """
    delete comments from storage
    Args:
        request: str, request from user
        pk: int or str, primary key for storage
    Returns:
        redirect to view names 'thank'
        (see file views.py: url(..., name='thank'))
    """
    post = get_object_or_404(Comment, pk=pk)
    post.delete()
    return redirect('thank')

