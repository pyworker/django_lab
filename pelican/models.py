from django.db import models
from django.utils import timezone

# Create your models here.

class Comment(models.Model):
    """
    datamodel for works with users comment
    """
    #
    # Fields
    #

    # limited field
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    
    # not limited field
    comment = models.TextField()
    
    # bolean field
    like = models.BooleanField()

    # date field
    published_date = models.DateTimeField(default=timezone.now)

    #
    # Methods
    #
    def publish(self):
        """
        publish comment
        """
        self.published_date = timezone.now()
        self.save()