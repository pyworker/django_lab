"""
module for describe all applications forms
"""
from django import forms


class NameForm(forms.Form):
    """
    specify fields and their types for user filling
    """
    your_name = forms.CharField(label='Your name', max_length=100)
    comment = forms.CharField(label='Your comment', widget=forms.Textarea)
    email_address = forms.EmailField()
    like = forms.BooleanField(required=False)