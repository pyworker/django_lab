from django.apps import AppConfig


class PelicanConfig(AppConfig):
    name = 'pelican'
