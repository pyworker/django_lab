"""
module for implementing the
interface with COMMENT datamodel 
"""

from .models import Comment

class IComment:
    """interface for work with comments"""
    def __init__(self):
        pass

    def create_comment(self, name, comment, email, like):
        """
        work with ORM for create add new comment to Database 
        Returns:
            True: if data added to database
        """
        Comment.objects.create(name=name, email=email, comment=comment, like=like)
        return True

    def get_all_comments(self):
        """
        get all comments from database
        Returns:
            list of comments from database
        """
        commnets = Comment.objects.all()
        return commnets
        